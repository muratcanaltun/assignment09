package Assignment08;

import javax.swing.*;
import java.awt.*;

public class TripleWithRadioButtonPanel extends JPanel {
    public TripleWithRadioButtonPanel() {
        JLabel label = new JLabel("Hi.");
        JTextField textField = new JTextField(20);
        JButton button1 = new JButton("Append");
        JButton button2 = new JButton("Reset");
        JRadioButton low = new JRadioButton("lowercase");
        JRadioButton up = new JRadioButton("UPPERCASE");

        ButtonGroup upDown = new ButtonGroup();
        upDown.add(low);
        upDown.add(up);

        add(label);
        add(textField);
        add(button1);
        add(button2);
        add(low);
        add(up);
    }
}
